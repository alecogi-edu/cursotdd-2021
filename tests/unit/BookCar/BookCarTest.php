<?php

namespace Test\Unit\BookCar;

use PHPUnit\Framework\TestCase;
use Service\CarNotFoundException;
use UseCase\BookCar;
use Model\User;
use Model\Booking;
use Service\CarFinder;
use Model\Car;
use Service\DbConnection;
use UseCase\CarNotAvailableException;
use UseCase\MinorsCannotBookCarsException;

class BookCarTest extends TestCase {

    /**
     * @test
     */
    public function adultsCanBookAvailableCars()
    {
        $userStub = $this->createStub(User::class);
        $userStub->method('getId')->willReturn(1);
        $userStub->method('isAnAdult')->willReturn(true);

        $dbConnectionStub = $this->createStub(DbConnection::class);
        $dbConnectionStub->method('insert')->willReturn(1);

        $carStub = $this->createStub(Car::class);
        $carStub->method('isAvailable')->willReturn(true);

        $carFinderStub = $this->createStub(CarFinder::class);

        $carFinderStub->method('find')->willReturn($carStub);

        $bookCarUseCase  = new BookCar($carFinderStub, $dbConnectionStub);
        $booking = $bookCarUseCase->execute(
            $userStub,
            1
        );

        $this->assertInstanceOf(Booking::class, $booking);
    }

    /**
     * @test
     */
    public function adultsCanNotBookUnavailableCars()
    {
        $userStub = $this->createStub(User::class);
        $userStub->method('isAnAdult')->willReturn(true);

        $dbConnectionDummy = $this->createStub(DbConnection::class);

        $carStub = $this->createStub(Car::class);
        $carStub->method('isAvailable')->willReturn(false);

        $carFinderStub = $this->createStub(CarFinder::class);
        $carFinderStub->method('find')->willReturn($carStub);

        $this->expectException(CarNotAvailableException::class);
        $bookCarUseCase = new BookCar($carFinderStub, $dbConnectionDummy);
        $bookCarUseCase->execute(
            $userStub,
            1
        );
    }

    /**
     * @test
     */
    public function adultsCanNotBookNonExistentCars()
    {
        $userStub = $this->createStub(User::class);
        $userStub->method('isAnAdult')->willReturn(true);

        $dbConnectionDummy = $this->createStub(DbConnection::class);
        $carFinderStub = $this->createStub(CarFinder::class);
        $carFinderStub->method('find')
            ->willThrowException(new CarNotFoundException(-1));

        $bookCarUseCase = new BookCar($carFinderStub, $dbConnectionDummy);
        $this->expectException(CarNotFoundException::class);
        $bookCarUseCase->execute(
            $userStub,
            1
        );
    }

    /**
     * @test
     */
    public function minorsCannotBookAvailableCars()
    {
        $dbConnectionDummy = $this->createStub(DbConnection::class);
        $carFinderStub = $this->createStub(CarFinder::class);

        $userStub = $this->createStub(User::class);
        $userStub->method('isAnAdult')->willReturn(false);

        $carStub = $this->createStub(Car::class);
        $carStub->method('isAvailable')->willReturn(true);

        $carFinderStub->method('find')->willReturn($carStub);

        $bookCarUseCase = new BookCar($carFinderStub, $dbConnectionDummy);
        $this->expectException(MinorsCannotBookCarsException::class);
        $bookCarUseCase->execute(
            $userStub,
            1
        );
    }

    /**
     * @test
     */
    public function minorsCannotBookUnAvailableCars()
    {
        $dbConnectionDummy = $this->createStub(DbConnection::class);

        $carStub = $this->createStub(Car::class);
        $carStub->method('isAvailable')->willReturn(false);

        $carFinderStub = $this->createStub(CarFinder::class);
        $carFinderStub->method('find')->willReturn($carStub);

        $userStub = $this->createStub(User::class);
        $userStub->method('isAnAdult')->willReturn(false);

        $bookCarUseCase = new BookCar($carFinderStub, $dbConnectionDummy);
        $this->expectException(MinorsCannotBookCarsException::class);
        $bookCarUseCase->execute(
            $userStub,
            1
        );
    }

    /**
     * @test
     */
    public function minorsCannotBookNonExistingCars()
    {
        $dbConnectionDummy = $this->createStub(DbConnection::class);

        $carFinderStub = $this->createStub(CarFinder::class);
        $carFinderStub->method('find')->willThrowException(new CarNotFoundException(-1));

        $userStub = $this->createStub(User::class);
        $userStub->method('isAnAdult')->willReturn(false);

        $bookCarUseCase = new BookCar($carFinderStub, $dbConnectionDummy);

        $this->expectException(MinorsCannotBookCarsException::class);
        $bookCarUseCase->execute(
            $userStub,
            1
        );
    }
}