<?php
namespace UseCase;

use Exception;

class CarNotAvailableException extends Exception
{
    public function __construct()
    {
        parent::__construct("El coche que intentas reservar no esta disponible");
    }
}