<?php declare(strict_types=1);
namespace UseCase;

use Exception;

class MinorsCannotBookCarsException extends Exception
{
}