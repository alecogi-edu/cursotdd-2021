<?php

namespace Service;

class DbConnection {

    private string $conn;


    /**
     * @throws NonValidConnectionException
     */
    public function __construct(string $host, string $dbUser, string $dbPass, string $dbName)
    {
        $this->conn = new \mysqli($host, $dbUser, $dbPass, $dbName);

        if ($this->connect_error) {
            throw new NonValidConnectionException();
        }
    }

    /**
     * @throws InsertException
     */
    public function insert(string $sql): int{
        $inserted = $this->conn->query($sql);
        if(!$inserted){
            $this->close();
            throw new InsertException();
        }
        return $this->conn->insert_id;
    }

    public function query(string $sql): array
    {
        $registers = [];
        if ($result = $this->conn->query($sql)) {
            while($row = $result->fetch_object()){
                $registers[] = $row;
            }
        }
        $result->close();
        return $registers;
    }

    public function close(){
        $this->conn->close();
    }
}