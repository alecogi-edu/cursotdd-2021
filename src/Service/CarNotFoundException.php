<?php
namespace Service;

use Exception;

class CarNotFoundException extends Exception
{
    public function __construct(int $id)
    {
        parent::__construct("El Coche con identificador {$id} no ha sido encontrado");
    }
}